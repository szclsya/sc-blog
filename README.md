# sc-blog

Pelican source for Leo's blog.

## Function status

| Function | Status | Comment |
|---|---|---|
| archives | Done | |
| article | Done | |
| author  | Done | |
| authors | Not started | |
| base | Done | |
| categories | Not started | |
|category | Not started | |
| gosquared | Not started | |
| index | Done | Need extra tune |
| page| Done | Need extra tune |
| pagination| Done ||
| period_archives | Not started | |
| tag | Not started | |
| tags | Not started | |
| translation | Not started | |

## Technology used

* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)