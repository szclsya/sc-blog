// Nav bar check
function checkNav() {
  if (window.pageYOffset < 271) {
    if (NavStatus !== 11) {
      setNav(1);
      NavStatus = 11;
    }
  }
  if (window.pageYOffset > 270) {
    if (NavStatus !== 12) {
      setNav(2);
      NavStatus = 12;
    }
  }
}

function setNav(status) {
  if (status == 1) {
    navBar.classList.add("sc-transbar");
    navBar.classList.add("z-depth-0");
    navBar.classList.remove("light-blue");
    navBar.classList.remove("darken-1");
    toTop.style.top = "100px"
  }
  if (status == 2) {
    navBar.classList.remove("sc-transbar");
    navBar.classList.remove("z-depth-0");
    navBar.classList.add("light-blue");
    navBar.classList.add("darken-1");
    toTop.style.top = "0px"
  }
}

//Progress bar set
function progressOn(WantOn) {
  if (WantOn) {
    progressBar.style.display = 'block';
  }
  if (!WantOn) {
    progressBar.style.display = 'none';
  }
}

// Convert rem to px
function remToPx(rem) {
  return parseInt(window.getComputedStyle(document.documentElement)['font-size'])*rem;
}

// Mobile detector  https://stackoverflow.com/questions/12259701/how-do-i-prevent-a-script-from-running-on-mobile-devices
function isMobile() {
  return /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

mainMenu = M.Sidenav.init(document.querySelector('.sidenav'));
rightTopToolbox = M.Dropdown.init(document.querySelector('.dropdown-trigger'), { constrainWidth: false });
progressBar = document.querySelector('.progress');
navBar = document.querySelector('#sc-nav');
toTop = document.querySelector('#toTop');
var scroll = new SmoothScroll('a[href*="#"]', {
  header: '.navbar-fixed'
});

M.AutoInit();

var NavStatus = 0; // 0 - Undefinded, 1 - Transparent, 2 - Color, 11 - TransparentSet, 12 - ColorSet
checkNav();

window.addEventListener('scroll', checkNav);
window.onload = progressOn(false);

