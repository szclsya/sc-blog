// ScrollSpy

var contentList = document.getElementById('content').getElementsByClassName('section');
var tocList = document.getElementById('sc-toc').getElementsByTagName('a');
// Content Location
var contentLoc = [];
for (j = 0, len = contentList.length; j < len; j++) {
    // Add pageYOffset to prevent the page is not at top when load
    contentLoc[j] = document.getElementById(contentList[j].id).getBoundingClientRect().top + window.pageYOffset;
}


var locNo = -1;
var tempLocNo = getPosition(contentLoc);
var progressBar = document.getElementById('sc-tocindicator');

function spyPosition() {
    tempLocNo = getPosition(contentLoc);
    if (tempLocNo != locNo && locNo != -1) {
        changeActive(tempLocNo, locNo);
        locNo = tempLocNo;
    }
    if (locNo == -1) {
        tocList[tempLocNo].classList.add('active');
        locNo = tempLocNo;
    }
}

function getPosition(contentLoc) {
    currentLoc = window.pageYOffset + 100;
    for (j = 0, len = contentLoc.length - 1; j < len; j++) {
        if (currentLoc > contentLoc[j] && currentLoc < contentLoc[j + 1]) {
            tempLocNo = j;
            break;
        }
    }
    if (currentLoc > contentLoc[contentLoc.length - 1]) {
        tempLocNo = contentLoc.length - 1;
    }
    if (currentLoc < contentLoc[0]) {
        tempLocNo = 0;
    }
    return tempLocNo;
}

function changeActive(desti, origin) {
    tocList[desti].classList.add('active');
    tocList[origin].classList.remove('active');
    progressBar.style.top = tocList[desti].offsetTop + 'px';
    progressBar.style.height = tocList[desti].clientHeight + 'px';

}

// Sticky table of contents
var topLoc = remToPx(40) - 64;
var bottomLoc = document.getElementById('content').getBoundingClientRect().bottom + window.pageYOffset;
tocLoc = null;
newTocLoc = null;

function stickyToc() {
    currentLoc = window.pageYOffset;
    if (currentLoc < topLoc) {
        newTocLoc = 0;
    }
    if (currentLoc > topLoc && currentLoc < bottomLoc) {
        newTocLoc = 1;
    }
    if (currentLoc > bottomLoc) {
        newTocLoc = 2;
    }
    if (newTocLoc != tocLoc) {
        changeTocLoc(tocLoc, newTocLoc);
        tocLoc = newTocLoc;
    }
}

function changeTocLoc(originLoc, destiLoc) {
    document.getElementById('sc-toc').classList.remove('toc' + originLoc);
    document.getElementById('sc-toc').classList.add('toc' + destiLoc);
}

if (!isMobile()) {
    stickyToc();
    spyPosition();
    document.getElementById( 'sc-tocindicator' ).style.display = 'inline';

    window.addEventListener('scroll', spyPosition);
    window.addEventListener('scroll', stickyToc);
}